PhoenixGen
==========

PhoenixGen provides a set of tools for generating [Phoenix](https://hexdocs.pm/phoenix)
projects and other pieces within them.

Roadmap:

- [x] Initial version of the "Service" Generator for JSON based microservices
- [ ] Add libraries for API calls
- [ ] Add GraphQL capabilities using [Absinthe](https://hexdocs.pm/absinthe/overview.html)
- [ ] Add "Frontend" Generator for [Drab-based](https://hexdocs.pm/drab/Drab.html) (or maybe LiveView) frontend projects