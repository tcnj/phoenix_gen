defmodule PhoenixGen.Tools.MixProject do
  use Mix.Project

  def project do
    [
      app: :phoenix_gen_tools,
      version: "0.2.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:httpoison, "~> 1.5"},
      {:jason, "~> 1.1"},
      {:ex_doc, "~> 0.14", only: :dev}
    ]
  end

  defp description do
    "PhoenixGen Tools provides tools to assist in Phoenix Framework projects, both at runtime and during development."
  end

  defp package do
    [
      name: "phoenix_gen_tools",
      licenses: ["MIT"],
      source_url: "https://gitlab.com/tcnj/phoenix_gen/tree/master/tools",
      links: %{
        "GitLab" => "https://gitlab.com/tcnj/phoenix_gen/tree/master/tools"
      }
    ]
  end
end
