# PhoenixGen Tools

PhoenixGen Tools provides tools to assist in Phoenix Framework projects, both at runtime and during development.

## Installation

The package can be installed
by adding `phoenix_gen_tools` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:phoenix_gen_tools, "~> 0.2.0"}
  ]
end
```

Documentation can be found at [https://hexdocs.pm/phoenix_gen_tools](https://hexdocs.pm/phoenix_gen_tools).

