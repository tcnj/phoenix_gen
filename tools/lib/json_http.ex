defmodule JsonHttp do
  use HTTPoison.Base

  @moduledoc """
  Provide a HTTPoison module that automatically parses for a JSON response
  """

  def process_response_body(body) do
    body
    |> Jason.decode!
  end
end