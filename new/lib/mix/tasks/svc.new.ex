defmodule Mix.Tasks.Phxg.Svc.New do
  use Mix.Task

  import Mix.Generator

  @shortdoc "Creates a new service project"
  
  @moduledoc """
  Generates a new phoenix-based JSON microservice project.

      mix phxg.svc.new PATH [--module MODULE] [--app APP]

  This is similar to the default phoenix project, except the assets directory is removed,
  various other HTML-related things such as the templates folder are removed, and various
  things have been adjusted for JSON-only output. A default controller and view is also
  put in place that serves up simple information about the project in response to GET /

  `--module MODULE` sets the module to use for the business logic. A module name for the
  web-side is derived from this.

  `--app APP` changes the OTP Application name.

  ## Examples

      mix phxg.svc.new minimal_service
      mix phxg.svc.new minimal_service --module MinimalService --app minimal_service
  """

  @switches [
    app: :string,
    module: :string
  ]

  @tools_version "0.2.0"
  
  @tools_local Path.join(System.cwd(), "../tools") |> Path.expand()
  @tools_mix_version if(Mix.env == :prod, do: "\"#{@tools_version}\"", else: "path: \"#{@tools_local}\"")

  @impl true
  def run(argv) do
    {opts, argv} = OptionParser.parse!(argv, strict: @switches)

    case argv do
      [] ->
        Mix.raise("Expected PATH to be given, please use \"mix phxg.svc.new PATH\"")

      [path | _] ->
        app = opts[:app] || Path.basename(Path.expand(path))
        check_application_name!(app, !opts[:app])
        mod = opts[:module] || Macro.camelize(app)
        check_mod_name_validity!(mod)
        check_mod_name_availability!(mod)

        unless path == "." do
          check_directory_existence!(path)
          File.mkdir_p!(path)
        end

        File.cd!(path, fn ->
          generate(app, mod, path, opts)
        end)
    end
  end

  defp check_application_name!(name, inferred?) do
    unless name =~ Regex.recompile!(~r/^[a-z][a-z0-9_]*$/) do
      Mix.raise(
        "Application name must start with a lowercase ASCII letter, followed by " <>
          "lowercase ASCII letters, numbers, or underscores, got: #{inspect(name)}" <>
          if inferred? do
            ". The application name is inferred from the path, if you'd like to " <>
              "explicitly name the application then use the \"--app APP\" option"
          else
            ""
          end
      )
    end
  end

  defp check_mod_name_validity!(name) do
    unless name =~ Regex.recompile!(~r/^[A-Z]\w*(\.[A-Z]\w*)*$/) do
      Mix.raise(
        "Module name must be a valid Elixir alias (for example: Foo.Bar), got: #{inspect(name)}"
      )
    end
  end

  defp check_mod_name_availability!(name) do
    name = Module.concat(Elixir, name)

    if Code.ensure_loaded?(name) do
      Mix.raise("Module name #{inspect(name)} is already taken, please choose another name")
    end
  end

  defp check_directory_existence!(path) do
    msg = "The directory #{inspect(path)} already exists. Are you sure you want to continue?"

    if File.dir?(path) and not Mix.shell().yes?(msg) do
      Mix.raise("Please select another directory for installation")
    end
  end

  defp get_version(version) do
    {:ok, version} = Version.parse(version)

    "#{version.major}.#{version.minor}" <>
      case version.pre do
        [h | _] -> "-#{h}"
        [] -> ""
      end
  end

  defp generate(app, mod, _path, _opts) do
    mod_filename = Macro.underscore(mod)

    assigns = [
      app: app,
      mod: mod,
      mod_filename: mod_filename,
      version: get_version(System.version())
    ]

    create_file("README.md", readme_template(assigns))
    create_file(".formatter.exs", formatter_template(assigns))
    create_file(".gitignore", gitignore_template(assigns))
    create_file("mix.exs", mix_exs_template(assigns))

    create_directory("config")
    
    create_file("config/config.exs", config_config_template(assigns))
    create_file("config/dev.exs", config_dev_template(assigns))
    create_file("config/prod.exs", config_prod_template(assigns))
    create_file("config/prod.secret.exs", config_prod_secret_template(assigns))
    create_file("config/test.exs", config_test_template(assigns))

    create_directory("lib")

    create_file("lib/#{mod_filename}.ex", module_template(assigns))
    create_file("lib/#{mod_filename}_web.ex", module_web_template(assigns))

    create_directory("lib/#{mod_filename}")

    create_file("lib/#{mod_filename}/application.ex", application_template(assigns))
    create_file("lib/#{mod_filename}/repo.ex", repo_template(assigns))

    create_directory("lib/#{mod_filename}_web")

    create_file("lib/#{mod_filename}_web/router.ex", router_template(assigns))
    create_file("lib/#{mod_filename}_web/endpoint.ex", endpoint_template(assigns))

    create_directory("lib/#{mod_filename}_web/controllers")

    create_file("lib/#{mod_filename}_web/controllers/root_controller.ex", root_controller_template(assigns))

    create_directory("lib/#{mod_filename}_web/views")

    create_file("lib/#{mod_filename}_web/views/root_view.ex", root_view_template(assigns))
    create_file("lib/#{mod_filename}_web/views/error_view.ex", error_view_template(assigns))

    create_directory("priv")
    create_directory("priv/repo")

    create_file("priv/repo/seeds.exs", repo_seeds_template(assigns))

    create_directory("priv/repo/migrations")

    create_file("priv/repo/migrations/.formatter.exs", repo_migrations_formatter_template(assigns))

    create_directory("test")

    create_file("test/test_helper.exs", test_helper_template(assigns))

    create_directory("test/#{mod_filename}_web")
    create_directory("test/#{mod_filename}_web/controllers")

    create_file("test/#{mod_filename}_web/controllers/root_controller_test.exs", root_controller_test_template(assigns))

    create_directory("test/#{mod_filename}_web/views")

    create_file("test/#{mod_filename}_web/views/error_view_test.exs", error_view_test_template(assigns))
    create_file("test/#{mod_filename}_web/views/root_view_test.exs", root_view_test_template(assigns))

    create_directory("test/support")

    create_file("test/support/conn_case.ex", conn_case_template(assigns))
    create_file("test/support/data_case.ex", data_case_template(assigns))

    """
    Your Mix project was created successfully.

    Run mix deps.get inside the project directory to fetch dependencies.
    """
    |> String.trim_trailing()
    |> Mix.shell().info()
  end

  embed_template(:formatter, """
  # Used by "mix format"
  [
    import_deps: [:ecto, :phoenix],
    inputs: ["*.{ex,exs}", "priv/*/seeds.exs", "{config,lib,test}/**/*.{ex,exs}"],
    subdirectories: ["priv/*/migrations"]
  ]
  """)

  embed_template(:gitignore, """
  # The directory Mix will write compiled artifacts to.
  /_build/

  # If you run "mix test --cover", coverage assets end up here.
  /cover/

  # The directory Mix downloads your dependencies sources to.
  /deps/

  # Where 3rd-party dependencies like ExDoc output generated docs.
  /doc/

  # Ignore .fetch files in case you like to edit your project deps locally.
  /.fetch

  # If the VM crashes, it generates a dump, let's ignore it too.
  erl_crash.dump

  # Also ignore archive artifacts (built via "mix archive.build").
  *.ez

  # Ignore package tarball (built via "mix hex.build").
  <%= @app %>-*.tar

  # Files matching config/*.secret.exs pattern contain sensitive
  # data and you should not commit them into version control.
  #
  # Alternatively, you may comment the line below and commit the
  # secrets files as long as you replace their contents by environment
  # variables.
  /config/*.secret.exs
  """)

  embed_template(:mix_exs, """
  defmodule <%= @mod %>.MixProject do
    use Mix.Project
  
    def project do
      [
        app: :<%= @app %>,
        version: "0.1.0",
        elixir: "~> <%= @version %>",
        elixirc_paths: elixirc_paths(Mix.env()),
        compilers: [:phoenix] ++ Mix.compilers(),
        start_permanent: Mix.env() == :prod,
        aliases: aliases(),
        deps: deps()
      ]
    end
  
    # Configuration for the OTP application.
    #
    # Type `mix help compile.app` for more information.
    def application do
      [
        mod: {<%= @mod %>.Application, []},
        extra_applications: [:logger, :runtime_tools]
      ]
    end
  
    # Specifies which paths to compile per environment.
    defp elixirc_paths(:test), do: ["lib", "test/support"]
    defp elixirc_paths(_), do: ["lib"]
  
    # Specifies your project dependencies.
    #
    # Type `mix help deps` for examples and options.
    defp deps do
      [
        {:phoenix, "~> 1.4.0"},
        {:phoenix_ecto, "~> 4.0"},
        {:ecto_sql, "~> 3.0"},
        {:postgrex, ">= 0.0.0"},
        {:plug_cowboy, "~> 2.0"},
        {:phoenix_gen_tools, #{@tools_mix_version}}
      ]
    end
  
    # Aliases are shortcuts or tasks specific to the current project.
    # For example, to create, migrate and run the seeds file at once:
    #
    #     $ mix ecto.setup
    #
    # See the documentation for `Mix` for more info on aliases.
    defp aliases do
      [
        "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
        "ecto.reset": ["ecto.drop", "ecto.setup"],
        test: ["ecto.create --quiet", "ecto.migrate", "test"]
      ]
    end
  end  
  """)

  embed_template(:readme, """
  # <%= @mod %>

  To start your Phoenix server:

    * Install dependencies with `mix deps.get`
    * Create and migrate your database with `mix ecto.setup`
    * Start Phoenix endpoint with `mix phx.server`

  Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

  Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

  ## Learn more

    * Official website: http://www.phoenixframework.org/
    * Guides: https://hexdocs.pm/phoenix/overview.html
    * Docs: https://hexdocs.pm/phoenix
    * Mailing list: http://groups.google.com/group/phoenix-talk
    * Source: https://github.com/phoenixframework/phoenix
  """)

  embed_template(:config_config, """
  # This file is responsible for configuring your application
  # and its dependencies with the aid of the Mix.Config module.
  #
  # This configuration file is loaded before any dependency and
  # is restricted to this project.

  # General application configuration
  use Mix.Config

  config :<%= @app %>,
    ecto_repos: [<%= @mod %>.Repo]

  # Configures the endpoint
  config :<%= @app %>, <%= @mod %>Web.Endpoint,
    url: [host: "localhost"],
    secret_key_base: "xTh5rzB5huzG5p9iWOf2zzYgo+RqEDhJ3izPC9WHmrHw0H0sfcqkOtffSt1zwhcQ",
    render_errors: [view: <%= @mod %>Web.ErrorView, accepts: ~w(json)]

  # Configures Elixir's Logger
  config :logger, :console,
    format: "$time $metadata[$level] $message\n",
    metadata: [:request_id]

  # Use Jason for JSON parsing in Phoenix
  config :phoenix, :json_library, Jason

  # Import environment specific config. This must remain at the bottom
  # of this file so it overrides the configuration defined above.
  import_config "\#{Mix.env()}.exs"
  """)

  embed_template(:config_dev, """
  use Mix.Config

  # For development, we disable any cache and enable
  # debugging and code reloading.
  #
  # The watchers configuration can be used to run external
  # watchers to your application. For example, we use it
  # with webpack to recompile .js and .css sources.
  config :<%= @app %>, <%= @mod %>Web.Endpoint,
    http: [port: 4000],
    debug_errors: true,
    check_origin: false

  # ## SSL Support
  #
  # In order to use HTTPS in development, a self-signed
  # certificate can be generated by running the following
  # Mix task:
  #
  #     mix phx.gen.cert
  #
  # Note that this task requires Erlang/OTP 20 or later.
  # Run `mix help phx.gen.cert` for more information.
  #
  # The `http:` config above can be replaced with:
  #
  #     https: [
  #       port: 4001,
  #       cipher_suite: :strong,
  #       keyfile: "priv/cert/selfsigned_key.pem",
  #       certfile: "priv/cert/selfsigned.pem"
  #     ],
  #
  # If desired, both `http:` and `https:` keys can be
  # configured to run both http and https servers on
  # different ports.

  # config :<%= @app %>, <%= @mod %>Web.Endpoint

  # Do not include metadata nor timestamps in development logs
  config :logger, :console, format: "[$level] $message\n"

  # Set a higher stacktrace during development. Avoid configuring such
  # in production as building large stacktraces may be expensive.
  config :phoenix, :stacktrace_depth, 20

  # Initialize plugs at runtime for faster development compilation
  config :phoenix, :plug_init_mode, :runtime

  # Configure your database
  config :<%= @app %>, <%= @mod %>.Repo,
    username: "postgres",
    password: "postgres",
    database: "<%= @app %>_dev",
    hostname: "localhost",
    pool_size: 10
  """)

  embed_template(:config_prod, """
  use Mix.Config

  # For production, don't forget to configure the url host
  # to something meaningful, Phoenix uses this information
  # when generating URLs.
  #
  # Note we also include the path to a cache manifest
  # containing the digested version of static files. This
  # manifest is generated by the `mix phx.digest` task,
  # which you should run after static files are built and
  # before starting your production server.
  config :<%= @app %>, <%= @mod %>Web.Endpoint,
    http: [:inet6, port: System.get_env("PORT") || 4000],
    url: [host: "example.com", port: 80]

  # Do not print debug messages in production
  config :logger, level: :info

  # ## SSL Support
  #
  # To get SSL working, you will need to add the `https` key
  # to the previous section and set your `:url` port to 443:
  #
  #     config :<%= @app %>, <%= @mod %>Web.Endpoint,
  #       ...
  #       url: [host: "example.com", port: 443],
  #       https: [
  #         :inet6,
  #         port: 443,
  #         cipher_suite: :strong,
  #         keyfile: System.get_env("SOME_APP_SSL_KEY_PATH"),
  #         certfile: System.get_env("SOME_APP_SSL_CERT_PATH")
  #       ]
  #
  # The `cipher_suite` is set to `:strong` to support only the
  # latest and more secure SSL ciphers. This means old browsers
  # and clients may not be supported. You can set it to
  # `:compatible` for wider support.
  #
  # `:keyfile` and `:certfile` expect an absolute path to the key
  # and cert in disk or a relative path inside priv, for example
  # "priv/ssl/server.key". For all supported SSL configuration
  # options, see https://hexdocs.pm/plug/Plug.SSL.html#configure/1
  #
  # We also recommend setting `force_ssl` in your endpoint, ensuring
  # no data is ever sent via http, always redirecting to https:
  #
  #     config :<%= @app %>, <%= @mod %>Web.Endpoint,
  #       force_ssl: [hsts: true]
  #
  # Check `Plug.SSL` for all available options in `force_ssl`.

  # ## Using releases (distillery)
  #
  # If you are doing OTP releases, you need to instruct Phoenix
  # to start the server for all endpoints:
  #
  #     config :phoenix, :serve_endpoints, true
  #
  # Alternatively, you can configure exactly which server to
  # start per endpoint:
  #
  #     config :<%= @app %>, <%= @mod %>Web.Endpoint, server: true
  #
  # Note you can't rely on `System.get_env/1` when using releases.
  # See the releases documentation accordingly.

  # Finally import the config/prod.secret.exs which should be versioned
  # separately.
  import_config "prod.secret.exs"
  """)

  embed_template(:config_prod_secret, """
  use Mix.Config

  # In this file, we keep production configuration that
  # you'll likely want to automate and keep away from
  # your version control system.
  #
  # You should document the content of this
  # file or create a script for recreating it, since it's
  # kept out of version control and might be hard to recover
  # or recreate for your teammates (or yourself later on).
  config :<%= @app %>, <%= @mod %>Web.Endpoint,
    secret_key_base: "2Oiueqw8lCYejD2E+tDsezKGeRCodT9CzntwpvPIe1VDfs7rZkHb4a3yYKAUeeMq"

  # Configure your database
  config :<%= @app %>, <%= @mod %>.Repo,
    username: "postgres",
    password: "postgres",
    database: "<%= @app %>_prod",
    pool_size: 15
  """)

  embed_template(:config_test, """
  use Mix.Config

  # We don't run a server during test. If one is required,
  # you can enable the server option below.
  config :<%= @app %>, <%= @mod %>Web.Endpoint,
    http: [port: 4002],
    server: false

  # Print only warnings and errors during test
  config :logger, level: :warn

  # Configure your database
  config :<%= @app %>, <%= @mod %>.Repo,
    username: "postgres",
    password: "postgres",
    database: "<%= @app %>_test",
    hostname: "localhost",
    pool: Ecto.Adapters.SQL.Sandbox
  """)

  embed_template(:module, """
  defmodule <%= @mod %> do
    @moduledoc \"""
    <%= @mod %> keeps the contexts that define your domain
    and business logic.
  
    Contexts are also responsible for managing your data, regardless
    if it comes from the database, an external API or others.
    \"""
  end
  """)

  embed_template(:module_web, """
  defmodule <%= @mod %>Web do
    @moduledoc \"""
    The entrypoint for defining your web interface, such
    as controllers, views, channels and so on.
  
    This can be used in your application as:
  
        use <%= @mod %>Web, :controller
        use <%= @mod %>Web, :view
  
    The definitions below will be executed for every view,
    controller, etc, so keep them short and clean, focused
    on imports, uses and aliases.
  
    Do NOT define functions inside the quoted expressions
    below. Instead, define any helper function in modules
    and import those modules here.
    \"""
  
    def controller do
      quote do
        use Phoenix.Controller, namespace: <%= @mod %>Web
  
        import Plug.Conn
        alias <%= @mod %>Web.Router.Helpers, as: Routes
      end
    end
  
    def view do
      quote do
        use Phoenix.View,
          root: "lib/<%= @app %>_web/templates",
          namespace: <%= @mod %>Web
  
        # Import convenience functions from controllers
        import Phoenix.Controller, only: [get_flash: 1, get_flash: 2, view_module: 1]
  
        alias <%= @mod %>Web.Router.Helpers, as: Routes
      end
    end
  
    def router do
      quote do
        use Phoenix.Router
        import Plug.Conn
        import Phoenix.Controller
      end
    end
  
    def channel do
      quote do
        use Phoenix.Channel
      end
    end
  
    @doc \"""
    When used, dispatch to the appropriate controller/view/etc.
    \"""
    defmacro __using__(which) when is_atom(which) do
      apply(__MODULE__, which, [])
    end
  end  
  """)

  embed_template(:application, """
  defmodule <%= @mod %>.Application do
    # See https://hexdocs.pm/elixir/Application.html
    # for more information on OTP Applications
    @moduledoc false
  
    use Application
  
    def start(_type, _args) do
      # List all child processes to be supervised
      children = [
        # Start the Ecto repository
        <%= @mod %>.Repo,
        # Start the endpoint when the application starts
        <%= @mod %>Web.Endpoint
        # Starts a worker by calling: <%= @mod %>.Worker.start_link(arg)
        # {<%= @mod %>.Worker, arg},
      ]
  
      # See https://hexdocs.pm/elixir/Supervisor.html
      # for other strategies and supported options
      opts = [strategy: :one_for_one, name: <%= @mod %>.Supervisor]
      Supervisor.start_link(children, opts)
    end
  
    # Tell Phoenix to update the endpoint configuration
    # whenever the application is updated.
    def config_change(changed, _new, removed) do
      <%= @mod %>Web.Endpoint.config_change(changed, removed)
      :ok
    end
  end  
  """)

  embed_template(:repo, """
  defmodule <%= @mod %>.Repo do
    use Ecto.Repo,
      otp_app: :<%= @app %>,
      adapter: Ecto.Adapters.Postgres
  end  
  """)

  embed_template(:router, """
  defmodule <%= @mod %>Web.Router do
    use <%= @mod %>Web, :router
  
    pipeline :api do
      plug :accepts, ["json"]
    end
  
    scope "/", <%= @mod %>Web do
      pipe_through :api
  
      get "/", RootController, :index
    end
  end  
  """)

  embed_template(:endpoint, """
  defmodule <%= @mod %>Web.Endpoint do
    use Phoenix.Endpoint, otp_app: :<%= @app %>
  
    plug Plug.RequestId
    plug Plug.Logger
  
    plug Plug.Parsers,
      parsers: [:urlencoded, :multipart, :json],
      pass: ["*/*"],
      json_decoder: Phoenix.json_library()
  
    plug Plug.MethodOverride
    plug Plug.Head
  
    # The session will be stored in the cookie and signed,
    # this means its contents can be read but not tampered with.
    # Set :encryption_salt if you would also like to encrypt it.
    plug Plug.Session,
      store: :cookie,
      key: "_<%= @app %>_key",
      signing_salt: "TBiCMKWV"
  
    plug <%= @mod %>Web.Router
  end  
  """)

  embed_template(:root_controller, """
  defmodule <%= @mod %>Web.RootController do
    use <%= @mod %>Web, :controller
  
    def index(conn, _params) do
      render(conn, :index)
    end
  end  
  """)

  embed_template(:error_view, """
  defmodule <%= @mod %>Web.ErrorView do
    use <%= @mod %>Web, :view
  
    # If you want to customize a particular status code
    # for a certain format, you may uncomment below.
    # def render("500.html", _assigns) do
    #   "Internal Server Error"
    # end
  
    # By default, Phoenix returns the status message from
    # the template name. For example, "404.html" becomes
    # "Not Found".
    def template_not_found(template, _assigns) do
      Phoenix.Controller.status_message_from_template(template)
    end
  end  
  """)

  embed_template(:root_view, """
  defmodule <%= @mod %>Web.RootView do
    use <%= @mod %>Web, :view
  
    def render("index.json", _assigns) do
      %{
        data: %{
          service: "<%= @app %>",
          version: <%= @mod %>.MixProject.project()[:version]
        },
        success: true
      }
    end
  end  
  """)

  embed_template(:repo_seeds, """
  # Script for populating the database. You can run it as:
  #
  #     mix run priv/repo/seeds.exs
  #
  # Inside the script, you can read and write to any of your
  # repositories directly:
  #
  #     <%= @mod %>.Repo.insert!(%<%= @mod %>.SomeSchema{})
  #
  # We recommend using the bang functions (`insert!`, `update!`
  # and so on) as they will fail if something goes wrong.
  """)

  embed_template(:repo_migrations_formatter, """
  [
    import_deps: [:ecto_sql],
    inputs: ["*.exs"]
  ]  
  """)

  embed_template(:test_helper, """
  ExUnit.start()
  Ecto.Adapters.SQL.Sandbox.mode(<%= @mod %>.Repo, :manual)
  """)

  embed_template(:root_controller_test, """
  defmodule <%= @mod %>Web.RootControllerTest do
    use <%= @mod %>Web.ConnCase
  
    test "GET /", %{conn: conn} do
      conn = get(conn, "/")
      assert json_response(conn, 200)["success"]
    end
  end  
  """)

  embed_template(:error_view_test, """
  defmodule <%= @mod %>Web.ErrorViewTest do
    use <%= @mod %>Web.ConnCase, async: true
  
    # Bring render/3 and render_to_string/3 for testing custom views
    import Phoenix.View
  
    test "renders 404.html" do
      assert render_to_string(<%= @mod %>Web.ErrorView, "404.html", []) == "Not Found"
    end
  
    test "renders 500.html" do
      assert render_to_string(<%= @mod %>Web.ErrorView, "500.html", []) == "Internal Server Error"
    end
  end  
  """)

  embed_template(:root_view_test, """
  defmodule <%= @mod %>Web.RootViewTest do
    use <%= @mod %>Web.ConnCase, async: true
  end  
  """)

  embed_template(:conn_case, """
  defmodule <%= @mod %>Web.ConnCase do
    @moduledoc \"""
    This module defines the test case to be used by
    tests that require setting up a connection.
  
    Such tests rely on `Phoenix.ConnTest` and also
    import other functionality to make it easier
    to build common data structures and query the data layer.
  
    Finally, if the test case interacts with the database,
    it cannot be async. For this reason, every test runs
    inside a transaction which is reset at the beginning
    of the test unless the test case is marked as async.
    \"""
  
    use ExUnit.CaseTemplate
  
    using do
      quote do
        # Import conveniences for testing with connections
        use Phoenix.ConnTest
        alias <%= @mod %>Web.Router.Helpers, as: Routes
  
        # The default endpoint for testing
        @endpoint <%= @mod %>Web.Endpoint
      end
    end
  
    setup tags do
      :ok = Ecto.Adapters.SQL.Sandbox.checkout(<%= @mod %>.Repo)
  
      unless tags[:async] do
        Ecto.Adapters.SQL.Sandbox.mode(<%= @mod %>.Repo, {:shared, self()})
      end
  
      {:ok, conn: Phoenix.ConnTest.build_conn()}
    end
  end  
  """)

  embed_template(:data_case, """
  defmodule <%= @mod %>.DataCase do
    @moduledoc \"""
    This module defines the setup for tests requiring
    access to the application's data layer.
  
    You may define functions here to be used as helpers in
    your tests.
  
    Finally, if the test case interacts with the database,
    it cannot be async. For this reason, every test runs
    inside a transaction which is reset at the beginning
    of the test unless the test case is marked as async.
    \"""
  
    use ExUnit.CaseTemplate
  
    using do
      quote do
        alias <%= @mod %>.Repo
  
        import Ecto
        import Ecto.Changeset
        import Ecto.Query
        import <%= @mod %>.DataCase
      end
    end
  
    setup tags do
      :ok = Ecto.Adapters.SQL.Sandbox.checkout(<%= @mod %>.Repo)
  
      unless tags[:async] do
        Ecto.Adapters.SQL.Sandbox.mode(<%= @mod %>.Repo, {:shared, self()})
      end
  
      :ok
    end
  
    @doc \"""
    A helper that transforms changeset errors into a map of messages.
  
        assert {:error, changeset} = Accounts.create_user(%{password: "short"})
        assert "password is too short" in errors_on(changeset).password
        assert %{password: ["password is too short"]} = errors_on(changeset)
  
    \"""
    def errors_on(changeset) do
      Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
        Enum.reduce(opts, message, fn {key, value}, acc ->
          String.replace(acc, "%{\#{key}}", to_string(value))
        end)
      end)
    end
  end  
  """)
end