defmodule PhoenixGen.New.MixProject do
  use Mix.Project

  def project do
    [
      app: :phoenix_gen_new,
      version: "0.2.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.14", only: :dev}
    ]
  end

  defp description do
    "PhoenixGen New provides some project generators for different types of phoenix projects to help you get off the ground quicker."
  end

  defp package do
    [
      name: "phoenix_gen_new",
      licenses: ["MIT"],
      source_url: "https://gitlab.com/tcnj/phoenix_gen/tree/master/new",
      links: %{
        "GitLab" => "https://gitlab.com/tcnj/phoenix_gen/tree/master/new"
      }
    ]
  end
end
